<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reff_sales_type".
 *
 * @property int $sales_type
 * @property string $sales_type_def
 * @property string $comp_id
 */
class SalesType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reff_sales_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comp_id'], 'required'],
            [['sales_type_def'], 'string', 'max' => 40],
            [['comp_id'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sales_type' => 'Sales Type',
            'sales_type_def' => 'Sales Type Def',
            'comp_id' => 'Comp ID',
        ];
    }
}
