<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "spv_team".
 *
 * @property string $team_id
 * @property string $spv_id
 * @property string $sales_id
 * @property bool $active
 * @property int $judgment_spv
 * @property string $crtdt
 *
 * @property SamUsers $spv
 * @property SamUsers $sales
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spv_team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'spv_id', 'sales_id', 'active'], 'required'],
            [['active'], 'boolean'],
            [['judgment_spv'], 'default', 'value' => null],
            [['judgment_spv'], 'integer'],
            [['crtdt'], 'safe'],
            [['team_id', 'spv_id', 'sales_id'], 'string', 'max' => 40],
            [['team_id', 'spv_id', 'sales_id'], 'unique', 'targetAttribute' => ['team_id', 'spv_id', 'sales_id']],
            [['spv_id'], 'exist', 'skipOnError' => true, 'targetClass' => SamUsers::className(), 'targetAttribute' => ['spv_id' => 'user_id']],
            [['sales_id'], 'exist', 'skipOnError' => true, 'targetClass' => SamUsers::className(), 'targetAttribute' => ['sales_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'team_id' => 'Team ID',
            'spv_id' => 'Spv ID',
            'sales_id' => 'Sales ID',
            'active' => 'Active',
            'judgment_spv' => 'Judgment Spv',
            'crtdt' => 'Crtdt',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpv()
    {
        return $this->hasOne(SamUsers::className(), ['user_id' => 'spv_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales()
    {
        return $this->hasOne(SamUsers::className(), ['user_id' => 'sales_id']);
    }
}
