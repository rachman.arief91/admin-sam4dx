<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reff_bill_status".
 *
 * @property int $bill_status
 * @property string $bill_status_def
 *
 * @property Company[] $companies
 */
class ReffBillStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reff_bill_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bill_status'], 'required'],
            [['bill_status'], 'default', 'value' => null],
            [['bill_status'], 'integer'],
            [['bill_status_def'], 'string'],
            [['bill_status'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bill_status' => 'Bill Status',
            'bill_status_def' => 'Bill Status Def',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['bill_status' => 'bill_status']);
    }
}
