<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reff_city".
 *
 * @property string $province_code
 * @property string $city_code
 * @property string $city_name
 * @property integer $rank
 *
 * @property Customer[] $customers
 * @property ReffBranch[] $reffBranches
 * @property ReffProvince $provinceCode
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reff_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['province_code', 'city_code', 'city_name'], 'required'],
            [['rank'], 'integer'],
            [['province_code'], 'string', 'max' => 4],
            [['city_code'], 'string', 'max' => 5],
            [['city_name'], 'string', 'max' => 60],
            [['province_code', 'city_code'], 'unique', 'targetAttribute' => ['province_code', 'city_code'], 'message' => 'The combination of Province Code and City Code has already been taken.'],
            [['province_code'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_code' => 'province_code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'province_code' => 'Province Code',
            'city_code' => 'City Code',
            'city_name' => 'City Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['province_code' => 'province_code', 'city_code' => 'city_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReffBranches()
    {
        return $this->hasMany(Branch::className(), ['province_code' => 'province_code', 'city_code' => 'city_code']);
    }

    public function getProvince()
    {
        return $this->hasOne(Province::className(), ['province_code' => 'province_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

}
