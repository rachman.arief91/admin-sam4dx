<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reff_province".
 *
 * @property string $province_code
 * @property string $province_name
 * @property integer $rank
 *
 * @property ReffCity[] $reffCities
 */
class Province extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reff_province';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['province_code', 'province_name'], 'required'],
            [['rank'], 'integer'],
            [['province_code'], 'string', 'max' => 4],
            [['province_name'], 'string', 'max' => 40],
            [['province_code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'province_code' => 'Province Code',
            'province_name' => 'Province Name',
            'rank' => 'Rank',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReffCities()
    {
        return $this->hasMany(ReffCity::className(), ['province_code' => 'province_code']);
    }
}
