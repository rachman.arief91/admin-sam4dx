<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reff_branch".
 *
 * @property string $branch_code
 * @property string $comp_id
 * @property string $province_code
 * @property string $city_code
 * @property string $branch_name
 * @property string $branch_address
 * @property double $branch_lat
 * @property double $branch_lon
 * @property string $parent_branch
 * @property int $branch_type
 *
 * @property Prospect[] $prospects
 * @property Company $comp
 * @property ReffBranchType $branchType
 * @property ReffCity $provinceCode
 */
class ReffBranch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reff_branch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch_code', 'comp_id', 'province_code', 'city_code', 'branch_name', 'branch_type'], 'required'],
            [['branch_lat', 'branch_lon'], 'number'],
            [['branch_type'], 'default', 'value' => null],
            [['branch_type'], 'integer'],
            [['branch_code', 'comp_id', 'province_code', 'parent_branch'], 'string', 'max' => 4],
            [['city_code'], 'string', 'max' => 5],
            [['branch_name'], 'string', 'max' => 40],
            [['branch_address'], 'string', 'max' => 256],
            [['branch_code', 'comp_id'], 'unique', 'targetAttribute' => ['branch_code', 'comp_id']],
            [['comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['comp_id' => 'company_id']],
            [['branch_type'], 'exist', 'skipOnError' => true, 'targetClass' => ReffBranchType::className(), 'targetAttribute' => ['branch_type' => 'branch_type']],
            [['province_code', 'city_code'], 'exist', 'skipOnError' => true, 'targetClass' => ReffCity::className(), 'targetAttribute' => ['province_code' => 'province_code', 'city_code' => 'city_code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'branch_code' => 'Branch Code',
            'comp_id' => 'Comp ID',
            'province_code' => 'Province Code',
            'city_code' => 'City Code',
            'branch_name' => 'Nama Cabang',
            'branch_address' => 'Branch Address',
            'branch_lat' => 'Branch Lat',
            'branch_lon' => 'Branch Lon',
            'parent_branch' => 'Parent Branch',
            'branch_type' => 'Branch Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProspects()
    {
        return $this->hasMany(Prospect::className(), ['branch_code' => 'branch_code', 'comp_id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComp()
    {
        return $this->hasOne(Company::className(), ['company_id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranchType()
    {
        return $this->hasOne(ReffBranchType::className(), ['branch_type' => 'branch_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinceCode()
    {
        return $this->hasOne(ReffCity::className(), ['province_code' => 'province_code', 'city_code' => 'city_code']);
    }
}
