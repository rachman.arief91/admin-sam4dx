<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property string $cust_id
 * @property string $province_code
 * @property string $city_code
 * @property string $cust_name
 * @property string $identification_no
 * @property string $cust_address
 * @property string $cust_phone_no
 * @property string $cust_alt_phone_no
 * @property string $cust_crtdt
 *
 * @property ReffCity $provinceCode
 * @property Prospect[] $prospects
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cust_id', 'province_code', 'city_code'], 'required'],
            [['cust_crtdt'], 'safe'],
            [['cust_id', 'cust_name', 'identification_no'], 'string', 'max' => 40],
            [['province_code'], 'string', 'max' => 4],
            [['city_code'], 'string', 'max' => 5],
            [['cust_address'], 'string', 'max' => 80],
            [['cust_phone_no', 'cust_alt_phone_no'], 'string', 'max' => 16],
            [['cust_id'], 'unique'],
            [['province_code', 'city_code'], 'exist', 'skipOnError' => true, 'targetClass' => ReffCity::className(), 'targetAttribute' => ['province_code' => 'province_code', 'city_code' => 'city_code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cust_id' => 'Cust ID',
            'province_code' => 'Province Code',
            'city_code' => 'City Code',
            'cust_name' => 'Nama',
            'identification_no' => 'KTP',
            'cust_address' => 'Alamat',
            'cust_phone_no' => 'Cust Phone No',
            'cust_alt_phone_no' => 'Cust Alt Phone No',
            'cust_crtdt' => 'Cust Crtdt',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinceCode()
    {
        return $this->hasOne(ReffCity::className(), ['province_code' => 'province_code', 'city_code' => 'city_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProspects()
    {
        return $this->hasMany(Prospect::className(), ['cust_id' => 'cust_id']);
    }
}
