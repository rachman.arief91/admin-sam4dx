<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reff_product_type".
 *
 * @property int $product_type
 * @property string $product_type_def
 * @property string $comp_id
 *
 * @property Product[] $products
 * @property Company $comp
 */
class ReffProductType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reff_product_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_type_def', 'comp_id'], 'required'],
            [['product_type_def', 'comp_id'], 'safe'],
            [['product_type_def'], 'string', 'max' => 40],
            [['comp_id'], 'string', 'max' => 4],
            [['comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['comp_id' => 'company_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_type' => 'Product Type',
            'product_type_def' => 'Product Type Def',
            'comp_id' => 'Comp ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['product_type' => 'product_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['company_id' => 'comp_id']);
    }
}
