<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sales_target".
 *
 * @property string $sales_id
 * @property string $comp_id
 * @property string $product_id
 * @property int $bobot
 * @property int $target_nominal
 * @property int $year
 * @property int $month
 * @property string $crtdt
 * @property string $assignee
 * @property int $product_type
 * @property string $branch_code
 *
 * @property Company $comp
 * @property SamUsers $sales
 */
class SalesTarget extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sales_target';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sales_id', 'comp_id', 'product_id', 'year', 'month', 'assignee'], 'required'],
            [['bobot', 'target_nominal', 'year', 'month', 'product_type'], 'default', 'value' => null],
            [['bobot', 'target_nominal', 'year', 'month', 'product_type'], 'integer'],
            [['crtdt'], 'safe'],
            [['sales_id', 'assignee'], 'string', 'max' => 40],
            [['comp_id', 'product_id', 'branch_code'], 'string', 'max' => 4],
            [['sales_id', 'comp_id', 'product_id', 'year', 'month'], 'unique', 'targetAttribute' => ['sales_id', 'comp_id', 'product_id', 'year', 'month']],
            [['comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['comp_id' => 'company_id']],
            //[['sales_id'], 'exist', 'skipOnError' => true, 'targetClass' => SamUsers::className(), 'targetAttribute' => ['sales_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sales_id' => 'Sales ID',
            'comp_id' => 'Comp ID',
            'product_id' => 'Product ID',
            'bobot' => 'Bobot',
            'target_nominal' => 'Target Nominal',
            'year' => 'Year',
            'month' => 'Month',
            'crtdt' => 'Crtdt',
            'assignee' => 'Assignee',
            'product_type' => 'Product Type',
            'branch_code' => 'Branch Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComp()
    {
        return $this->hasOne(Company::className(), ['company_id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales()
    {
        return $this->hasOne(SamUsers::className(), ['user_id' => 'sales_id']);
    }
}
