<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reff_bill".
 *
 * @property int $billing_type
 * @property string $terms
 * @property int $price
 * @property string $package_name
 *
 * @property Company[] $companies
 */
class ReffBill extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reff_bill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['billing_type', 'terms', 'price', 'package_name'], 'required'],
            [['billing_type', 'price'], 'default', 'value' => null],
            [['billing_type', 'price'], 'integer'],
            [['terms', 'package_name'], 'string'],
            [['billing_type'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'billing_type' => 'Billing Type',
            'terms' => 'Terms',
            'price' => 'Price',
            'package_name' => 'Package Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['billing_type' => 'billing_type']);
    }
}
