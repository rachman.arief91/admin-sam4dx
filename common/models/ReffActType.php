<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reff_act_type".
 *
 * @property int $act_type
 * @property string $act_type_def
 *
 * @property Activity[] $activities
 */
class ReffActType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reff_act_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['act_type_def'], 'required'],
            [['act_type_def'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'act_type' => 'Act Type',
            'act_type_def' => 'Definition',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['act_type' => 'act_type']);
    }
}
