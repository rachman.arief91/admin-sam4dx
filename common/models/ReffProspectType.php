<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reff_prospect_type".
 *
 * @property int $prospect_type
 * @property string $prospect_type_def
 *
 * @property Prospect[] $prospects
 * @property ProspectTypeConfig[] $prospectTypeConfigs
 */
class ReffProspectType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reff_prospect_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prospect_type_def'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'prospect_type' => 'Prospect Type',
            'prospect_type_def' => 'Prospect Type Def',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProspects()
    {
        return $this->hasMany(Prospect::className(), ['prospect_type' => 'prospect_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProspectTypeConfigs()
    {
        return $this->hasMany(ProspectTypeConfig::className(), ['prospect_type' => 'prospect_type']);
    }
}
