<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property string $activity_id
 * @property int $act_type
 * @property string $prospect_id
 * @property int $result_code
 * @property int $status_code
 * @property string $act_crtdt
 * @property int $act_seq
 * @property string $result
 * @property int $nominal_upd
 * @property string $appointment_date
 * @property string $description
 * @property string $hours
 * @property string $act_updt
 * @property bool $active
 * @property bool $is_holiday
 * @property string $image_id
 * @property double $longitude
 * @property double $latitude
 * @property string $alifast_no
 *
 * @property Prospect $prospect
 * @property RefProspectStatus $statusCode
 * @property ReffActType $actType
 * @property ReffResult $resultCode
 * @property ActivityComment[] $activityComments
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['activity_id', 'act_type', 'prospect_id', 'result_code', 'status_code', 'act_seq'], 'required'],
            [['act_type', 'result_code', 'status_code', 'act_seq', 'nominal_upd'], 'default', 'value' => null],
            [['act_type', 'result_code', 'status_code', 'act_seq', 'nominal_upd'], 'integer'],
            [['act_crtdt', 'appointment_date', 'hours', 'act_updt'], 'safe'],
            [['active', 'is_holiday'], 'boolean'],
            [['longitude', 'latitude'], 'number'],
            [['activity_id', 'prospect_id'], 'string', 'max' => 40],
            [['result', 'description'], 'string', 'max' => 1024],
            [['image_id'], 'string', 'max' => 50],
            [['alifast_no'], 'string', 'max' => 255],
            [['activity_id'], 'unique'],
            [['prospect_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prospect::className(), 'targetAttribute' => ['prospect_id' => 'prospect_id']],
            [['status_code'], 'exist', 'skipOnError' => true, 'targetClass' => RefProspectStatus::className(), 'targetAttribute' => ['status_code' => 'status_code']],
            [['act_type'], 'exist', 'skipOnError' => true, 'targetClass' => ReffActType::className(), 'targetAttribute' => ['act_type' => 'act_type']],
            [['result_code'], 'exist', 'skipOnError' => true, 'targetClass' => ReffResult::className(), 'targetAttribute' => ['result_code' => 'result_code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'activity_id' => 'Activity ID',
            'act_type' => 'Act Type',
            'prospect_id' => 'Prospect ID',
            'result_code' => 'Result Code',
            'status_code' => 'Status Code',
            'act_crtdt' => 'Act Crtdt',
            'act_seq' => 'Act Seq',
            'result' => 'Result',
            'nominal_upd' => 'Nominal Upd',
            'appointment_date' => 'Appointment Date',
            'description' => 'Description',
            'hours' => 'Hours',
            'act_updt' => 'Act Updt',
            'active' => 'Active',
            'is_holiday' => 'Is Holiday',
            'image_id' => 'Image ID',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'alifast_no' => 'Alifast No',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProspect()
    {
        return $this->hasOne(Prospect::className(), ['prospect_id' => 'prospect_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusCode()
    {
        return $this->hasOne(RefProspectStatus::className(), ['status_code' => 'status_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActType()
    {
        return $this->hasOne(ReffActType::className(), ['act_type' => 'act_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultCode()
    {
        return $this->hasOne(ReffResult::className(), ['result_code' => 'result_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityComments()
    {
        return $this->hasMany(ActivityComment::className(), ['activity_id' => 'activity_id']);
    }
}
