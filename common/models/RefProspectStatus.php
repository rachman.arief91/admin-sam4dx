<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ref_prospect_status".
 *
 * @property int $status_code
 * @property string $status_def
 *
 * @property Activity[] $activities
 * @property Prospect[] $prospects
 */
class RefProspectStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_prospect_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_code', 'status_def'], 'required'],
            [['status_code'], 'default', 'value' => null],
            [['status_code'], 'integer'],
            [['status_def'], 'string', 'max' => 40],
            [['status_code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_code' => 'Status Code',
            'status_def' => 'Status Def',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['status_code' => 'status_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProspects()
    {
        return $this->hasMany(Prospect::className(), ['status_code' => 'status_code']);
    }
}
