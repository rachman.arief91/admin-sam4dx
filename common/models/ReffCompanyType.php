<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reff_company_type".
 *
 * @property int $company_type
 * @property string $company_type_def
 *
 * @property Company[] $companies
 */
class ReffCompanyType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reff_company_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_type_def'], 'required'],
            [['company_type_def'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_type' => 'Company Type',
            'company_type_def' => 'Company Type Def',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['company_type' => 'company_type']);
    }
}
