<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property string $company_id
 * @property integer $company_type
 * @property string $company_name
 * @property string $key_code
 * @property string $contract_start
 * @property string $contract_end
 * @property string $due_date
 * @property string $billing_date
 * @property string $api_url
 * @property integer $billing_type
 * @property string $address
 * @property integer $bill_status
 * @property string $comp_email
 * @property boolean $active
 * @property string $image_id
 *
 * @property ReffBill $billingType
 * @property ReffBillStatus $billStatus
 * @property ReffCompanyType $companyType
 * @property Product[] $products
 * @property ProspectTypeConfig[] $prospectTypeConfigs
 * @property ReffBranch[] $reffBranches
 * @property ReffProductType[] $reffProductTypes
 * @property SalesTarget[] $salesTargets
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'company_type', 'company_name'], 'required'],
            [['company_type', 'billing_type', 'bill_status'], 'integer'],
            [['contract_start', 'contract_end', 'due_date', 'billing_date'], 'safe'],
            [['active'], 'boolean'],
            [['company_id'], 'string', 'max' => 4],
            [['company_name'], 'string', 'max' => 80],
            [['key_code'], 'string', 'max' => 128],
            [['image_id'],'file'],
            [['api_url', 'address'], 'string', 'max' => 256],
            [['comp_email'], 'string', 'max' => 255],
            [['company_id'], 'unique'],
            [['billing_type'], 'exist', 'skipOnError' => true, 'targetClass' => ReffBill::className(), 'targetAttribute' => ['billing_type' => 'billing_type']],
            [['bill_status'], 'exist', 'skipOnError' => true, 'targetClass' => ReffBillStatus::className(), 'targetAttribute' => ['bill_status' => 'bill_status']],
            [['company_type'], 'exist', 'skipOnError' => true, 'targetClass' => ReffCompanyType::className(), 'targetAttribute' => ['company_type' => 'company_type']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company Code',
            'company_type' => 'Company Type',
            'company_name' => 'Company Name',
            'key_code' => 'Key Code',
            'contract_start' => 'Contract Start',
            'contract_end' => 'Contract End',
            'due_date' => 'Due Date',
            'billing_date' => 'Billing Date',
            'api_url' => 'Api Url',
            'billing_type' => 'Billing Type',
            'address' => 'Address',
            'bill_status' => 'Bill Status',
            'comp_email' => 'Email Company',
            'active' => 'Active',
            'image_id' => 'Image ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillingType()
    {
        return $this->hasOne(ReffBill::className(), ['billing_type' => 'billing_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillStatus()
    {
        return $this->hasOne(ReffBillStatus::className(), ['bill_status' => 'bill_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyType()
    {
        return $this->hasOne(ReffCompanyType::className(), ['company_type' => 'company_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProspectTypeConfigs()
    {
        return $this->hasMany(ProspectTypeConfig::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReffBranches()
    {
        return $this->hasMany(ReffBranch::className(), ['comp_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReffProductTypes()
    {
        return $this->hasMany(ReffProductType::className(), ['comp_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesTargets()
    {
        return $this->hasMany(SalesTarget::className(), ['comp_id' => 'company_id']);
    }
}
