<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reff_branch_type".
 *
 * @property integer $branch_type
 * @property string $branch_type_def
 *
 * @property ReffBranch[] $reffBranches
 */
class BranchType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reff_branch_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch_type'], 'required'],
            [['branch_type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'branch_type' => 'Branch Type',
            'branch_type_def' => 'Branch Type Def',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReffBranches()
    {
        return $this->hasMany(ReffBranch::className(), ['branch_type' => 'branch_type']);
    }
}
