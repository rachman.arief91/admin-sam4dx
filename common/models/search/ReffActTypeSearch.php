<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ReffActType;

/**
 * ReffActTypeSearch represents the model behind the search form of `common\models\ReffActType`.
 */
class ReffActTypeSearch extends ReffActType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['act_type'], 'integer'],
            [['act_type_def'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReffActType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'act_type' => $this->act_type,
        ]);

        $query->andFilterWhere(['ilike', 'act_type_def', $this->act_type_def]);

        return $dataProvider;
    }
}
