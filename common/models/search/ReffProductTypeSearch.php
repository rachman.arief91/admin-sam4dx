<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ReffProductType;

/**
 * ReffProductTypeSearch represents the model behind the search form of `common\models\ReffProductType`.
 */
class ReffProductTypeSearch extends ReffProductType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_type'], 'integer'],
            [['product_type_def', 'comp_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReffProductType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'product_type' => $this->product_type,
        ]);

        $query->andFilterWhere(['ilike', 'product_type_def', $this->product_type_def])
            ->andFilterWhere(['ilike', 'comp_id', $this->comp_id]);

        return $dataProvider;
    }
}
