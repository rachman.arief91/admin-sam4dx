<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Company;

/**
 * CompanySearch represents the model behind the search form of `common\models\Company`.
 */
class CompanySearch extends Company
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'company_name', 'key_code', 'contract_start', 'contract_end', 'due_date', 'billing_date', 'api_url', 'address', 'comp_email', 'image_id'], 'safe'],
            [['company_type', 'billing_type', 'bill_status'], 'integer'],
            [['active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'company_type' => $this->company_type,
            'contract_start' => $this->contract_start,
            'contract_end' => $this->contract_end,
            'due_date' => $this->due_date,
            'billing_date' => $this->billing_date,
            'billing_type' => $this->billing_type,
            'bill_status' => $this->bill_status,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['ilike', 'company_id', $this->company_id])
            ->andFilterWhere(['ilike', 'company_name', $this->company_name])
            ->andFilterWhere(['ilike', 'key_code', $this->key_code])
            ->andFilterWhere(['ilike', 'api_url', $this->api_url])
            ->andFilterWhere(['ilike', 'address', $this->address])
            ->andFilterWhere(['ilike', 'comp_email', $this->comp_email])
            ->andFilterWhere(['ilike', 'image_id', $this->image_id]);

        return $dataProvider;
    }
}
