<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sam_users".
 *
 * @property string $user_id
 * @property integer $role_id
 * @property string $email
 * @property string $passwd
 * @property string $nama
 * @property string $address
 * @property string $phone_no
 * @property double $cur_lat
 * @property double $cur_lon
 * @property string $fbs_token
 * @property string $profile_pic
 * @property boolean $active
 * @property integer $user_status
 * @property string $fcm_token
 * @property string $token
 * @property string $branch_code
 * @property string $comp_id
 * @property integer $sales_type
 *
 * @property Prospect[] $prospects
 * @property Prospect[] $prospects0
 * @property SamRole $role
 */
class SamUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sam_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'role_id', 'email', 'passwd', 'nama', 'address', 'phone_no', 'branch_code', 'comp_id'], 'required'],
            [['role_id', 'user_status', 'sales_type'], 'integer'],
            [['cur_lat', 'cur_lon'], 'number'],
            [['active'], 'boolean'],
            [['user_id'], 'string', 'max' => 40],
            [['email', 'nama'], 'string', 'max' => 80],
            [['passwd'], 'string', 'max' => 256],
            [['address', 'profile_pic'], 'string', 'max' => 128],
            [['phone_no'], 'string', 'max' => 16],
            [['fbs_token'], 'string', 'max' => 1024],
            [['fcm_token', 'token'], 'string', 'max' => 512],
            [['branch_code', 'comp_id'], 'string', 'max' => 4],
            [['user_id'], 'unique'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => SamRole::className(), 'targetAttribute' => ['role_id' => 'role_id']],
            //[['branch_code'], 'exist', 'skipOnError' => true, 'targetClass' => Branch::className(), 'targetAttribute' => ['branch_code' => 'branch_code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'Username',
            'role_id' => 'Role ID',
            'email' => 'Email',
            'passwd' => 'Passwd',
            'nama' => 'Name',
            'address' => 'Address',
            'phone_no' => 'Phone No',
            'cur_lat' => 'Cur Lat',
            'cur_lon' => 'Cur Lon',
            'fbs_token' => 'Fbs Token',
            'profile_pic' => 'Profile Pic',
            'active' => 'Active',
            'user_status' => 'User Status',
            'fcm_token' => 'Fcm Token',
            'token' => 'Token',
            'branch_code' => 'Branch Code',
            'comp_id' => 'Comp ID',
            'sales_type' => 'Sales Type',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProspects()
    {
        return $this->hasMany(Prospect::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(SamRole::className(), ['role_id' => 'role_id']);
    }

    public function getLeader(){
        return $this->hasMany(Team::className(),['spv_id' => 'user_id']);
    }

    public function getChild(){
        return $this->hasMany(Team::className(),['sales_id' => 'user_id']);
    }

    public function getCompany(){
        return $this->hasOne(Company::className(),['company_id' => 'comp_id']);
    }

    public function getBranch(){
        return $this->hasOne(Branch::className(),['branch_code' => 'branch_code']);
    }
}
