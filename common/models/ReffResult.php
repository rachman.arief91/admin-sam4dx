<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reff_result".
 *
 * @property int $result_code
 * @property string $result_def
 *
 * @property Activity[] $activities
 * @property Prospect[] $prospects
 */
class ReffResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reff_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['result_def'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'result_code' => 'Result Code',
            'result_def' => 'Result Def',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['result_code' => 'result_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProspects()
    {
        return $this->hasMany(Prospect::className(), ['result_code' => 'result_code']);
    }
}
