var alertRedInput = "#f3b616";
var defaultInput = "rgba(158, 44, 33, 1)";

function userNameValidation(usernameInput) {
    var username = document.getElementById("email");
    var error = document.getElementById("error-msg");
    error.innerHTML = ""
    username.setCustomValidity("");
    username.style.borderColor = defaultInput;
}

function passwordValidation(passwordInput) {
    var password = document.getElementById("passwd");
    var error = document.getElementById("error-msg");
    error.innerHTML = ""
    password.setCustomValidity("");
    password.style.borderColor = defaultInput;
}
$('input').keypress(function(e){
    var code = e.keyCode || e.which;
    if( code === 13 ) {
        e.preventDefault();
        $( "#login-btn" ).click();
        return false;
    };
});
$('#login-btn').on('click', function(event){
    event.preventDefault();
    var form = $('#formLogin');
    form.parsley().validate();

    if (!form.parsley().isValid()) {
      return false;
    }else{
        console.log('valid');
        $("#cssload-overlay").removeClass('hide').show().fadeIn(3600, function (){
            formData = new FormData($('form')[0]);
            var hash = sha1.create();
            hash.update(passwd.value);
            hash.hex();

            formData.set("passwd", hash)

            for (var pair of formData.entries()) {
                console.log(pair[0]+ ', ' + pair[1]); 
            }
            $.ajax({
                  processData: false,
                  contentType: false,
                  type: 'POST',
                  url: 'login',
                  data: formData,
                  success: function (response) {
                    $("#cssload-overlay").hide().fadeOut();
                     console.log(response);
                     console.log("code : " + response.rc);
                    if(response.rc == 0){
                        window.location.href = 'home';
                    }else{
                        document.getElementById('error-msg').innerHTML = response.rm
                        document.getElementById("passwd").value = ""
                    }
                  },
                  error: function(data){
                    $("#cssload-overlay").hide().fadeOut();
                    console.log(data);
                  }
                });
        });
    }
});
