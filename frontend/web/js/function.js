

function ajaxMenu(e, elm){
  e.preventDefault();
  var url = $(elm).attr('href').replace('#', '');
  // console.log(url);
  if(url){
    // window.history.pushState(url, "",base_url+'/'+url);
    $('.modal').modal('hide');
    $('.ui.dimmable #loader').dimmer('destroy');
    $('body .modals').remove();
    loadMenu(url);
    // window.history.pushState(url, "",base_url+'/'+url);
  }
}

$(window).on("popstate", function () {
  
  $('.modal').modal('hide');
  $('.ui.dimmable #loader').dimmer('destroy');
  $('body .modals').remove();
  
  loadMenu(event.state.url, true);
});


function loadMenu(url, skip){
  // console.log(url);
  // console.log('loadMenu');
  var state = {name: "name", page: 'History', url:url};
  if(typeof skip === 'undefined'){
    window.history.pushState(state, "History",base_url+'/'+url);
  }
  var html = $('#content').html();
  $('#content').empty();
  // $('chosen-select').chosen("destroy");
  $("#cssload-overlay").removeClass('hide').show().fadeIn(3600, function (){
    $.get(base_url+'/'+url, function(response, status, xhr){
      var res = $.parseJSON(response);
      //console.log(res);
      $("#cssload-overlay").hide().fadeOut();
      $('#content').html(res.content);
      $('.data-script').html(res.scripts);
      $('.data-modal').html(res.modals);

    }).fail(function(response) {
      $("#cssload-overlay").hide().fadeOut();
      console.log(response);
      if(response.status == 401){
        window.location.href = base_url;
      } else {
        alert('Terjadi Kesalahan');
        $('#content').html(html);
      }          
    });
  });
}

function showConfirmation(elm, idRoute){
  var rowId = $(elm).closest('tr').attr('id'),
    oncClick = typeof idRoute === 'undefined'?'del("' + rowId + '");':'del("' + rowId + '","'+idRoute+'");';
  
  $('#submitBtn').attr('onclick', oncClick);
  $('#confirmationModal').modal('show');
}

function showConfirmationDelete(elm, idRoute){ 
  console.log('url b : ' + idRoute);
  var exp = new RegExp("\\\\","g"); 
  idRoute = idRoute.replace(exp,"\\\\");
  console.log('url a : ' + idRoute);
  var rowId = $(elm).closest('tr').attr('id'),
    oncClick = typeof idRoute === 'undefined'?'del("' + rowId + '");':'del("' + rowId + '","'+idRoute+'");';
  $('#confirmDeleteBtn').attr('onclick', oncClick);
  $('#confirmDeleteModal').modal('show')
}

function targetPersalesConf(elm, idRoute){ 
  console.log('url b : ' + idRoute);
  var row = $(elm).closest('tr');
  var i = 0;
  var prodType = 0;

  row.find('td').each(function(){
    console.log(i);
    console.log($(this).html());

    i++;
  });

  var exp = new RegExp("\\\\","g"); 
  idRoute = idRoute.replace(exp,"\\\\");
  console.log('url a : ' + idRoute);
  var rowId = $(elm).closest('tr').attr('id'),
    oncClick = typeof idRoute === 'undefined'?'targetPersalesDel("' + rowId + '");':'targetPersalesDel("' + rowId + '","'+idRoute+'");';
  
  $('#confirmDeleteBtn').attr('onclick', oncClick);
  $('#confirmDeleteModal').modal('show')
}


function showConfirmationAppr(elm, idRoute){
  var oncClick = 'get("'+idRoute+'");';
  $('#submitBtn').attr('onclick', oncClick);
  $('#confirmationModal').modal('show');
}

function showConfirmationGen(elm, oncClick){
  // var oncClick = 'get("'+idRoute+'");';
  $('#submitBtn').attr('onclick', oncClick);
  $('#confirmationModal').modal('show');
}



function get(idRoute){
  var str = window.location.href,
      route = typeof idRoute === 'undefined'?$("#routeDelete").val():idRoute,
      url = base_url+ "/" + route ,
      param = '';
  if(str.indexOf('?') > -1){
    param = str.slice(str.indexOf('?'));
  }
  url = url + param;
  $('.modal').each(function(){
        $(this).modal('hide');
      });
  // $('#content').html(loadingGif);
  $("#cssload-overlay").removeClass('hide').show().fadeIn(3600, function (){
    $.get(url, function(response, status, xhr){
      $("#cssload-overlay").hide().fadeOut();
      if(response.rc == 0){
        toastr.success('Sukses');
        // loadMenu(res.url);
        dataTable.draw();
      } else {
        toastr.error(response.rm);
      }

    }).fail(function(response) {
      $("#cssload-overlay").hide().fadeOut();
      toastr.error('Terjadi Kesalahan');
    });
  });
}

function editDT(elm, url){
  var str = window.location.href,
      id = $(elm).closest('tr').attr('id')
      param = '';

  if(str.indexOf('?') > -1){
    param = str.slice(str.indexOf('?'));
  }
  loadMenu(url+ "/" +id+param);
}

function nestedAdd(elm, url){
  var str = window.location.href,
      id = $(elm).closest('tr').attr('id')
      param = '';

  if(str.indexOf('?') > -1){
    param = str.slice(str.indexOf('?'));
  }

  loadMenu(url+"&id="+id);
}

function crudDatatabel(){
  if( $('.maker-usage').length > 0 ) {
    var str = window.location.href,
        param = '';
    if(str.indexOf('?') > -1){
      param = str.slice(str.indexOf('?'));
    }
    console.log("Param : " + param);
    $.get(base_url+'/screen/column-info'+param, function(resp){
      var res = JSON.parse(resp);
      dataTable = $('.maker-usage').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url" : base_url+'/screen/list'+param,
          "type": "GET"
        },
        "drawCallback" : function(settings){
          var page = dataTable.page.info();
          dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            console.log(page.page);
            cell.classList.add("collapsing");
            cell.classList.add("center");
            cell.classList.add("aligned");
            cell.innerHTML = (10*page.page)+(i+1);
          });
        },
        "columns": res.columns,
        "autoWidth":true,
        scrollX:        true,
        deferRender:    true,
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
          },
          {
            "searchable": false,
            "orderable": true,
            "visible":false,
            "targets": 1
          },
          {
            "targets": res.targets,
            "data": null,
            "searchable":false,
            "render" : function ( data, type, row, meta ) {
              var button = "";
              if(row.id_wf_status !== 'undefined' && row.id_wf_status == 0){
                button = res.button;
              }
              return button;
            }
          } 
        ],
        "order": [[ 1, "desc" ]],
      });
    });
  }
}

function apprDatatabel(){
  if( $('.appr-usage').length > 0 ) {
    var str = window.location.href,
        param = '';
    if(str.indexOf('?') > -1){
      param = str.slice(str.indexOf('?'));
    }
    
    $.get(base_url+'/screen/column-info'+param, function(resp){
      // console.log(JSON.parse());
      var res = JSON.parse(resp);
      dataTable = $('.appr-usage').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url" : base_url+'/screen/list'+param,
          "type": "GET"
        },
        "drawCallback" : function(settings){
          var page = dataTable.page.info();
          dataTable.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            // console.log(cell);
            console.log(page.page);
            cell.innerHTML = (10*page.page)+(i+1);
          });
        },
        // "columnDefs": resp.columnDefs,
        "columns": res.columns,
        "columnDefs": [ 
          {
             'targets': 0,
             'searchable': false,
             'orderable': false,
             'checkboxes': {
                'selectRow': true
             }
          },
          {
            "searchable": false,
            "orderable": false,
            "targets": 1
          },
          {
            "searchable": false,
            "orderable": true,
            "visible":false,
            "targets": 2
          },
          {
            "targets": res.targets,
            "data": null,
            "searchable":false,
            "render" : function ( data, type, row, meta ) {
              var button = res.button;
              return button;
              
            }
          } 
        ],
        'select': {
          'style': 'multi'
        },
        "order": [[ 2, "desc" ]],
      });
    });
  }
}

function qDatatabel(){
  if( $('.queue-usage').length > 0 ) {
    var str = window.location.href,
        param = '';
    if(str.indexOf('?') > -1){
      param = str.slice(str.indexOf('?'));
    }    
      
    dataTable = $('.queue-usage').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url" : base_url+'/upload/list'+param,
        "type": "GET"
      },
      "drawCallback" : function(settings){
        var page = dataTable.page.info();
          dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            // console.log(cell);
            console.log(page.page);
            cell.innerHTML = (10*page.page)+(i+1);
          });
      },
      "columns": [
        { data: null, sortable: false, searchable: false  },
        { data: 'crtdt', name: 'crtdt', orderable: true, searchable: false  },
        { data: 'filename', name: 'filename', orderable: true, searchable: false  },
        { data: 'upload_type', name: 'upload_type', orderable: true, searchable: true  },
        { data: 'nama_upload_status', name: 'nama_upload_status', orderable: true, searchable: false  }
      ],
      "columnDefs": [ {
          "searchable": false,
          "orderable": false,
          "targets": 0
        }
      ],
      "order": [[1, "desc" ]],
    });
  }
}

function prokerDatatabel(){
  if( $('.proker-usage').length > 0 ) {
    var str = window.location.href,
        param = '';
    if(str.indexOf('?') > -1){
      param = str.slice(str.indexOf('?'));
    }
    
    $.get(base_url+'/screen/column-info'+param, function(resp){
      // console.log(JSON.parse());
      var res = JSON.parse(resp);
      dataTable = $('.proker-usage').DataTable({
        "processing": true,
        "serverSide": true,
        scrollX:        true,
        "ajax": {
          "url" : base_url+'/screen/list'+param,
          "type": "GET"
        },
        "drawCallback" : function(settings){
          var page = dataTable.page.info();
          dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            // console.log(cell);
            console.log(page.page);
            cell.innerHTML = (10*page.page)+(i+1);
          });
        },
        // "columnDefs": resp.columnDefs,
        "columns": res.columns,
        "columnDefs": [ 
          {
            "searchable": false,
            "orderable": false,
            "targets": 0
          },
          {
            "searchable": false,
            "orderable": false,
            "visible":false,
            "targets": 1
          },
          { width: 300, targets: 4 },
          {
            "targets": res.targets,
            "data": null,
            "searchable":false,
            "render" : function ( data, type, row, meta ) {
              var button = res.button;
              return button;
              
            }
          } 
        ],
        "order": [[ 2, "desc" ]],
        fixedColumns: true
      });
    });
  }
}

function wdtDatatabel(){
  if( $('.wdt-usage').length > 0 ) {
    var str = window.location.href,
        param = '';
    if(str.indexOf('?') > -1){
      param = str.slice(str.indexOf('?'));
    }
    $.get(base_url+'/screen/column-info'+param, function(resp){
      var res = JSON.parse(resp);
      dataTable = $('.wdt-usage').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url" : base_url+'/screen/list'+param,
          "type": "GET"
        },
        "drawCallback" : function(settings){
          var page = dataTable.page.info();
          dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            console.log(page.page);
            cell.classList.add("collapsing");
            cell.classList.add("center");
            cell.classList.add("aligned");
            cell.innerHTML = (10*page.page)+(i+1);
          });
        },
        "columns": res.columns,
        "autoWidth":true,
        scrollX:        true,
        deferRender:    true,
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
          },
          {
            "targets": res.targets,
            "data": null,
            "searchable":false,
            "render" : function ( data, type, row, meta ) {
              var button = "";
              button = res.button;
              return button;
            }
          } 
        ],
        "order": [[ 1, "asc" ]],
      });
    });
  }
}

function standardDatatabel(){
  if( $('.standard-usage').length > 0 ) {
    var str = window.location.href,
        param = '';
    if(str.indexOf('?') > -1){
      param = str.slice(str.indexOf('?'));
    }
    
    $.get(base_url+'/screen/column-info'+param, function(resp){
      // console.log(JSON.parse());
      var res = JSON.parse(resp);
      dataTable = $('.standard-usage').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url" : base_url+'/screen/list'+param,
          "type": "GET"
        },
        "drawCallback" : function(settings){
          var page = dataTable.page.info();
          dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            // console.log(cell);
            console.log(page.page);
            cell.innerHTML = (10*page.page)+(i+1);
          });
        },
        // "columnDefs": resp.columnDefs,
        "columns": res.columns,
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
          },
          {
            "searchable": false,
            "orderable": true,
            "visible":false,
            "targets": 1
          },
          {
            "targets": res.targets,
            "data": null,
            "searchable":false,
            "render" : function ( data, type, row, meta ) {
              var button = "";
              button = res.button;

              return button;
              
            }
          } 
        ],
        "order": [[ 1, "desc" ]],
      });

      // dataTable.on( 'order.dt search.dt', function () {
      //   dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
      //     console.log(cell);
      //     console.log(i);
      //     cell.innerHTML = i+1;
      //   });
      // });
    });
  }
}
function del(id, idRoute){
  var str = window.location.href,
      route = typeof idRoute === 'undefined'?$("#routeDelete").val():idRoute,
      url = base_url+ "/delete/" + id,
      param = '?mdl='+idRoute;
  url = url + param;
  $('.modal').each(function(){
        $(this).modal('hide');
      });
  $("#cssload-overlay").removeClass('hide').show().fadeIn(3600, function (){
    $.get(url, function(response, status, xhr){
      $("#cssload-overlay").hide().fadeOut();
      if(response.rc == 0){
        toastr.success('Sukses');
        // loadMenu(res.url);
        dataTable.ajax.reload();
      } else {
        toastr.error(response.rm);
      }

    }).fail(function(response) {
      $("#cssload-overlay").hide().fadeOut();
      toastr.error('Terjadi Kesalahan');
    });
  });
}

function targetPersalesDel(id, idRoute){
  var str = window.location.href,
      route = typeof idRoute === 'undefined'?$("#routeDelete").val():idRoute,
      url = base_url+ "/delete/" + id,
      param = '?mdl='+idRoute;
  url = url + param;
  $('.modal').each(function(){
        $(this).modal('hide');
      });
  $("#cssload-overlay").removeClass('hide').show().fadeIn(3600, function (){
    $.get(url, function(response, status, xhr){
      $("#cssload-overlay").hide().fadeOut();
      if(response.rc == 0){
        toastr.success('Sukses');
        // loadMenu(res.url);
        dataTable.ajax.reload();
      } else {
        toastr.error(response.rm);
      }

    }).fail(function(response) {
      $("#cssload-overlay").hide().fadeOut();
      toastr.error('Terjadi Kesalahan');
    });
  });
}

function save(mdl, id){
  if ($(".currency").length>0) {
        $(".currency").each(function(){
          $(this).val($(this).autoNumeric('get'));
        });
    }
    console.log('url b : ' + mdl);
    mdl.replace('/\\/', "\\\\");
    console.log('url a : ' + mdl);
    var str = window.location.href,
        param = '?mdl='+mdl,
        url = base_url+'/save',
        form = $('#formEdit'),
        formData = new FormData(form[0]);
    if(id == undefined){
        url += param;
      } else {
        url += "/" + id + param;
      }
    form.parsley().validate();
    console.log("status form : " + form.parsley().isValid());
    console.log("url save : " + url);

    if (!form.parsley().isValid()) {
      return false;
    }else{
      if(formData.get('password') != null){
        var pass = formData.get('password')
        var hash = sha1.create();
        hash.update(pass);
        hash.hex();

        formData.set("password", hash)
      }
      $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('show');
      $("#btnSubmit").addClass('disabled');
      for (var pair of formData.entries()) {
          console.log(pair[0]+ ', ' + pair[1]); 
      }
      $.ajax({
        processData: false,
        contentType: false,
        type: 'POST',
        url: url,
        data: formData,
        success: function (res) {
          $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('hide');
          $("#btnSubmit").removeClass('disabled');
          if(res.rc == 0){
            toastr.success('Sukses');
              if(id == undefined){
                form[0].reset();
              } else {
                $('#modalEdit').modal("hide");
              }
            dataTable.ajax.reload();
            // loadMenu(res.url);
          } else {
            toastr.error(res.rm);
          }
        },
        error: function(data){
          $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('hide');
          $("#btnSubmit").removeClass('disabled');
          toastr.error('Terjadi Kesalahan');
        }
      });
    }
}

function saveUser(mdl, id){
  if ($(".currency").length>0) {
        $(".currency").each(function(){
          $(this).val($(this).autoNumeric('get'));
        });
    }
    console.log('url b : ' + mdl);
    mdl.replace('/\\/', "\\\\");
    console.log('url a : ' + mdl);
    var str = window.location.href,
        param = '?mdl='+mdl,
        url = base_url+'/saveUser',
        form = $('#formEdit'),
        formData = new FormData(form[0]);
    if(id == undefined){
        url += param;
      } else {
        url += "/" + id + param;
      }
    form.parsley().validate();
    console.log("status form : " + form.parsley().isValid());
    console.log("url save : " + url);

    if (!form.parsley().isValid()) {
      return false;
    }else{
      if(formData.get('password') != null){
        var pass = formData.get('password')
        var hash = sha1.create();
        hash.update(pass);
        hash.hex();

        formData.set("password", hash)
      }
      $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('show');
      $("#btnSubmit").addClass('disabled');
      for (var pair of formData.entries()) {
          console.log(pair[0]+ ', ' + pair[1]); 
      }
      $.ajax({
        processData: false,
        contentType: false,
        type: 'POST',
        url: url,
        data: formData,
        success: function (res) {
          $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('hide');
          $("#btnSubmit").removeClass('disabled');
          if(res.rc == 0){
            toastr.success('Sukses');
              if(id == undefined){
                form[0].reset();
              } else {
                $('#modalEdit').modal("hide");
              }
            dataTable.ajax.reload();
            // loadMenu(res.url);
          } else {
            toastr.error(res.rm);
          }
        },
        error: function(data){
          $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('hide');
          $("#btnSubmit").removeClass('disabled');
          toastr.error('Terjadi Kesalahan');
        }
      });
    }
}

function exportExcel(url, method){
  $('#form').attr('action', url);
  $('#form').attr('method', method);
  console.log($('#form input[type=search]').val());
  $('#search').val($('#form input[type=search]').val());
  $('#order').val(dataTable.order());
  $('#form').submit();
}

function upload(url){
  var fileIn = $("#fileToUpload")[0],
      formData = new FormData();
  //Has any file been selected yet?
  if (fileIn.files === undefined || fileIn.files.length == 0) {
      alert("Please select a file");
      return;
  }

  //We will upload only one file in this demo
  var file = fileIn.files[0];
  formData.append('excel_file', fileIn.files[0]);
  //Show the progress bar
  $("#progressbarDiv").removeClass('hide');

  $.ajax({
      url: base_url + '/' + url,
      type: "POST",
      data: formData,
      processData: false, //Work around #1
      contentType: false, //Work around #2
      success: function(res){
          $("#progressbarDiv").addClass('hide');
          if(res.rc == 0){
            toastr.success('Sukses');
            loadMenu(res.url);
          } else {
            // console.log(res.rm);
            // toastr.error(res.rm);
            $('.alert').removeClass('hide');
            $('#errorMsg').empty();
            $('#errorMsg').html(res.rm);
          }
          // toastr.success('Sukses');
          // loadMenu('upload_npl');
          // window.history.go(-2);
      },
      error: function(){alert("Failed");},
      //Work around #3
      xhr: function() {
          myXhr = $.ajaxSettings.xhr();
          if(myXhr.upload){
              myXhr.upload.addEventListener('progress',showProgress, false);
          } else {
              console.log("Upload progress is not supported.");
          }
          return myXhr;
      }
  });
}
function showProgress(evt) {
    if (evt.lengthComputable) {
        var percentComplete = (evt.loaded / evt.total) * 100;
        console.log(percentComplete);
        $('#progressbar').attr("aria-valuenow", percentComplete );
        $('#progressbar .sr-only').html(percentComplete +"% Complete");
    }  
}

function changeSelect(elm, elmChange, idReff){
  // console.log(url);
  // console.log('loadMenu');
  var id = $(elm).val();
  // console.log(id);
  var url = 'refference/'+id+"/"+idReff;
  var html = $('#content').html();
  $("#cssload-overlay").removeClass('hide').show().fadeIn(3600, function (){
    $.get(base_url+'/'+url, function(response, status, xhr){
     
      $("#cssload-overlay").hide().fadeOut();
      // console.log(res.content);
      $("select[name='"+elmChange+"']").empty().append(response).trigger("chosen:updated");
    }).fail(function(response) {
      $("#cssload-overlay").hide().fadeOut();
      console.log(response);
      if(response.status == 401){
        window.location.href = base_url;
      } else {
        alert('Terjadi Kesalahan');

        $('#content').html(html);
      }          
    });
  });
}

function changeKgtKantor(elm){
  if($(elm).val() == 4){
    $(elm).parent().next().removeClass('hide');  
  } else {
    $(elm).parent().next().addClass('hide');
  }
  
}

function sendSelected(url){
  var formData = new FormData(),
      str = window.location.href,
      param = '',
      selected = [];
  $('.modal').each(function(){
    $(this).modal('hide');
  });
  if(str.indexOf('?') > -1){
    param = str.slice(str.indexOf('?'));
  }
  url = url + param;  
  var rows_selected = dataTable.column(0).checkboxes.selected();
  // console.log(rows_selected);
  $("#cssload-overlay").removeClass('hide').show().fadeIn(3600, function (){
    $.each(rows_selected, function(index, rowId){
      // console.log(rowId);
      selected.push(rowId);
    });

    formData.append('selected', selected);
    $.ajax({
      processData: false,
      contentType: false,
      type: 'POST',
      url: url,
      data: formData,
      success: function (res) {
        $("#cssload-overlay").hide().fadeOut();
        // var res = JSON.parse(response);
          // console.log(base_url);
        if(res.rc == 0){
          toastr.success('Sukses');
          loadMenu(res.url);
        } else {
          // console.log(res.rm);
          toastr.error(res.rm);
        }
      },
      error: function(data){
        $("#cssload-overlay").hide().fadeOut();
        toastr.error('Terjadi Kesalahan');
      }
    });
  });

}

function referensi(elem){
        var stateID = $(elem).val();
        if(stateID) {
            $.ajax({
                url: base_url+'/select-city/'+stateID,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    console.log(data);
                    $('select[name="city_code"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="city_code"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                }
            });
        }else{
            $('select[name="id"]').empty();
        }
}

function refBranch(elem){
        var stateID = $(elem).val();
        if(stateID) {
            $.ajax({
                url: base_url+'/select-branch/'+stateID,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    console.log(data);
                    $('select[name="parent_branch"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="parent_branch"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                }
            });
        }else{
            $('select[name="id"]').empty();
        }
}

function branchUserTarget(elem){
        var stateID = $(elem).val();
        if(stateID) {
            $.ajax({
                url: base_url+'/select-brachtarget/'+stateID,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    console.log(data);
                    $('select[name="userId"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="userId"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                }
            });
        }else{
            $('select[name="id"]').empty();
        }
}

function productTarget(elem){
        var stateID = $(elem).val();
        if(stateID) {
            $.ajax({
                url: base_url+'/select-prodtarget/'+stateID,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    console.log(data);
                    $('select[name="productId"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="productId"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                }
            });
        }else{
            $('select[name="id"]').empty();
        }
}

function branchType(elem){
        var stateID = $(elem).val();
        if(stateID) {
            $.ajax({
                url: base_url+'/select-branch/'+stateID,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    console.log(data);
                    $('select[name="brach_code"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="brach_code"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                }
            });
        }else{
            $('select[name="id"]').empty();
        }
}

function branchUser(elem){
        var stateID = $(elem).val();
        if(stateID) {
            $.ajax({
                url: base_url+'/select-brchuser/'+stateID,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    console.log(data);
                    $('select[name="branchCode"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="branchCode"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                }
            });
        }else{
            $('select[name="id"]').empty();
        }
}

function reffLeader(elem){
        var stateID = $(elem).val();
        if(stateID) {
            $.ajax({
                url: base_url+'/select-leader/'+stateID,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    console.log(data);
                    $('select[name="leaderId"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="leaderId"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                }
            });
        }else{
            $('select[name="id"]').empty();
        }
}

function salesTypeReff(elem){
        var stateID = $(elem).val();
        if(stateID) {
            $.ajax({
                url: base_url+'/select-salestype/'+stateID,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    console.log(data);
                    $('select[name="salesType"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="salesType"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                }
            });
        }else{
            $('select[name="id"]').empty();
        }
}

function salesTypeChange(elem){
        var stateID = $(elem).val();
        if(stateID) {
            $.ajax({
                url: base_url+'/sales-typechange/'+stateID,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    console.log(data);
                    if(stateID == 1){
                      $('select[name="salesType"]').parent().show();
                      $.each(data, function(key, value) {
                          $('select[name="salesType"]').append('<option value="'+ key +'">'+ value +'</option>');
                      });
                    }else{

                      $('select[name="salesType"]').parent().hide();
                    }
                }
            });
        }else{
            $('select[name="id"]').empty();
        }
}



function modalAdd(url){
    $('#modalEdit').modal({closable  : false}).modal("show");
    $('#modalEditTitle').text("");
    $('#modalEditContent').html('<p></p><br><p></p><br><p></p><br>');
    $('#modalEditAction').html('<br>');
    $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('show');
    $.get(base_url+'/form?mdl='+url, function(response, status, xhr){
      $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('hide');
      var res = $.parseJSON(response);
      console.log(res.content);
      $('#modalEditTitle').text(res.title);
      $('#modalEditContent').html(res.content); 
      $('#modalEditAction').html(res.footer);
      MINOVATE.extra.init();
    }).fail(function(response) {
       $('#modalEdit').modal("hide");
       $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('hide');
      console.log(response);
      if(response.status == 401){
        window.location.href = base_url;
      } else {
        toastr.error('Terjadi Kesalahan');
      }          
    });
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function modalEdit(elm, url){
  var str = window.location.href,
      id = $(elm).closest('tr').attr('id')
        //Jika id di db = 0 itu terbaca null, maka di set id = 0 jika id = ''
        if(id == 'act_type'|| id == 'status_code' 
        || id == 'role_id' || id == 'result_code'){
           id = 0;
        }
      param = '?mdl='+url;
  console.log('url : ' + base_url+ "/form-edit/" +id+param);
  // loadMenu(url+ "/" +id+param);
   $('#modalEdit').modal({closable  : false}).modal("show");
    $('#modalEditTitle').text("");
    $('#modalEditContent').html('<p></p><br><p></p><br><p></p><br>');
    $('#modalEditAction').html('<br>');
    $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('show');
    $.get(base_url+ "/form-edit/" +id+param, function(response, status, xhr){
      $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('hide');
      var res = $.parseJSON(response);
      console.log(res.content);
      $('#modalEditTitle').text(res.title);
      $('#modalEditContent').html(res.content); 
      $('#modalEditAction').html(res.footer);
      MINOVATE.extra.init();
    }).fail(function(response) {
       $('#modalEdit').modal("hide");
       $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('hide');
      console.log(response);
      if(response.status == 401){
        window.location.href = base_url;
      } else {
        toastr.error('Terjadi Kesalahan');
      }          
    });
}

function modalEditUser(elm, url){
  var str = window.location.href,
      id = $(elm).closest('tr').attr('id')
        //Jika id di db = 0 itu terbaca null, maka di set id = 0 jika id = ''
        if(id == 'act_type'|| id == 'status_code' 
        || id == 'role_id' || id == 'result_code'){
           id = 0;
        }
      param = '?mdl='+url;
  console.log('url : ' + base_url+ "/form-edit-user/" +id+param);
  // loadMenu(url+ "/" +id+param);
   $('#modalEdit').modal({closable  : false}).modal("show");
    $('#modalEditTitle').text("");
    $('#modalEditContent').html('<p></p><br><p></p><br><p></p><br>');
    $('#modalEditAction').html('<br>');
    $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('show');
    $.get(base_url+ "/form-edit-user/" +id+param, function(response, status, xhr){
      $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('hide');
      var res = $.parseJSON(response);
      console.log(res.content);
      $('#modalEditTitle').text(res.title);
      $('#modalEditContent').html(res.content); 
      $('#modalEditAction').html(res.footer);
      MINOVATE.extra.init();
    }).fail(function(response) {
       $('#modalEdit').modal("hide");
       $('.ui.dimmable #loader').dimmer({closable: false}).dimmer('hide');
      console.log(response);
      if(response.status == 401){
        window.location.href = base_url;
      } else {
        toastr.error('Terjadi Kesalahan');
      }          
    });
}

function activation(elm, url){
  var str = window.location.href,
      id = $(elm).closest('tr').attr('id')
      param = '?mdl='+url;
    console.log('url : ' + base_url+ "/activation/" +id);  
    $.get(base_url+ "/activation/" +id, function(response, status, xhr){
      $("#cssload-overlay").hide().fadeOut();
      if(response.rc == 0){
        toastr.success('Sukses');
        // loadMenu(res.url);
        dataTable.ajax.reload();
      } else {
        toastr.error(response.rm);
      }
    }).fail(function(response) {
      $("#cssload-overlay").hide().fadeOut();
      toastr.error('Terjadi Kesalahan');
    });
}

function activateuser(elm, url){
  var str = window.location.href,
      id = $(elm).closest('tr').attr('id')
      param = '?mdl='+url;
    console.log('url : ' + base_url+ "/activateuser/" +id);  
    $.get(base_url+ "/activateuser/" +id, function(response, status, xhr){
      $("#cssload-overlay").hide().fadeOut();
      if(response.rc == 0){
        toastr.success('Sukses');
        // loadMenu(res.url);
        dataTable.ajax.reload();
      } else {
        toastr.error(response.rm);
      }
    }).fail(function(response) {
      $("#cssload-overlay").hide().fadeOut();
      toastr.error('Terjadi Kesalahan');
    });
}
