<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SamRole */

$this->title = 'Create Sam Role';
$this->params['breadcrumbs'][] = ['label' => 'Sam Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sam-role-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
