<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SamRoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sam Roles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sam-role-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sam Role', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'role_id',
            'role_description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
