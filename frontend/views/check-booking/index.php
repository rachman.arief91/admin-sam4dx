<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use \yii\widgets\Pjax;
use \yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CheckBookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Check Booking';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;

if($session->hasFlash('approve')){
    ?>
    <div class="alert alert-success">
        <strong>Sukses!</strong> Anda telah melakukan approval ALIFAST
    </div>
    <?php
}

if($session->hasFlash('reject')){
    ?>
    <div class="alert alert-success">
        <strong>Sukses!</strong> Anda telah melakukan rejecting ALIFAST
    </div>
    <?php
}
?>
<div class="check-booking-index">

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin()?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No'
            ],
            [
                'attribute' => 'branchCode.branch_name',
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'cust.identification_no',
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'cust.cust_name',
                'headerOptions' => ['class' => 'text-center']

            ],
            [
                'attribute' => 'cust.cust_address',
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'target_nominal',
                'format' => 'currency',
                'contentOptions' => ['class' => 'text-right'],
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'prospect_crtdt',
                'format' => 'date',
                'header' => 'Tanggal Prospek',
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'template' => "{approve} &nbsp; {reject} &nbsp; {ajax}",
                'buttons' => [
                        'approve' => function($url, $model){
                            //return '<a href="'.Url::to(['approve', 'id' => $model->prospect_id]).'"><span title="Approve" class="label label-primary"><i class="fa fa-check"></i></span></a>';
                            return Html::a('<span title="Approve" class="label label-primary"><i class="fa fa-check"></i></span>',['approve','id_prospect' => $model->prospect_id]);
                        },
                        'reject' => function($url, $model){
                            return Html::a('<span title="Reject" class="label label-danger"><i class="fa fa-times"></i></span>',['reject','id_prospect' => $model->prospect_id]);
                        },

                ]
            ],
        ],
    ]); ?>
    <?php
    Pjax::end();
    ?>
</div>

<?php
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
?>

<?php
Modal::end();
?>
