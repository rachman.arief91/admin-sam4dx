<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use \kartik\select2\Select2;
use \common\models\Branch;

/* @var $this yii\web\View */
/* @var $model common\models\CheckBookingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-booking-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]);
    $session = Yii::$app->session;
    ?>

  <?= $form->field($model, 'branch_code')->widget(Select2::className(),[
        'model' => $model,
        'attribute' => 'branch_code',
        'data' => ArrayHelper::map(Branch::find()->where(['comp_id' => $session->get('comp_id')])->all(),'branch_code','branch_name'),
        'options' => ['placeholder' => 'Choose Branch'],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
