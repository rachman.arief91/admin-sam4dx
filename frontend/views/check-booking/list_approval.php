<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CheckBookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Report Approval';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;

if($session->hasFlash('approve')){
    ?>
    <div class="alert alert-success">
        <strong>Sukses!</strong> Anda telah melakukan approval ALIFAST
    </div>
    <?php
}

if($session->hasFlash('reject')){
    ?>
    <div class="alert alert-success">
        <strong>Sukses!</strong> Anda telah melakukan rejecting ALIFAST
    </div>
    <?php
}

?>
<div class="check-booking-index">

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No'
            ],
            [
                'attribute' => 'branchCode.branch_name',
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'cust.identification_no',
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'cust.cust_name',
                'headerOptions' => ['class' => 'text-center']

            ],
            [
                'attribute' => 'cust.cust_address',
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'target_nominal',
                'format' => 'currency',
                'contentOptions' => ['class' => 'text-right'],
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'prospect_crtdt',
                'format' => 'date',
                'header' => 'Tanggal Prospek',
                'headerOptions' => ['class' => 'text-center']
            ],
            
        ],
    ]); ?>
</div>
