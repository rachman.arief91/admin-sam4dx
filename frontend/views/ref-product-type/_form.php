<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\ReffProductType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reff-product-type-form">

    <?php
    $session = Yii::$app->session;
    $form = ActiveForm::begin();
    ?>

    <?= $form->field($model, 'product_type_def')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'comp_id')->widget(Select2::className(),[
            'model' => $model,
            'attribute' => 'comp_id',
            'data' => ArrayHelper::map(\common\models\Company::find()->all(),'company_id','company_name'),
            'options' => ['placeholder' => 'Choose Company'],
            'pluginOptions' => [
                    'allowClear' => true
            ]
    ]) ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
