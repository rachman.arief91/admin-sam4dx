<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ReffProductType */

$this->title = 'Create Reff Product Type';
$this->params['breadcrumbs'][] = ['label' => 'Reff Product Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reff-product-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
