<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ReffProductType */

$this->title = $model->product_type;
$this->params['breadcrumbs'][] = ['label' => 'Reff Product Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reff-product-type-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->product_type], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->product_type], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'product_type',
            'product_type_def',
            'comp_id',
        ],
    ]) ?>

</div>
