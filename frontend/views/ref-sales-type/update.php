<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ReffSalesType */

$this->title = 'Update Reff Sales Type';
$this->params['breadcrumbs'][] = ['label' => 'Reff Sales Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sales_type, 'url' => ['view', 'sales_type' => $model->sales_type, 'comp_id' => $model->comp_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reff-sales-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
