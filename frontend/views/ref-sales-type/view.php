<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ReffSalesType */

$this->title = $model->sales_type;
$this->params['breadcrumbs'][] = ['label' => 'Reff Sales Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reff-sales-type-view">

    <p>
        <?= Html::a('Update', ['update', 'sales_type' => $model->sales_type, 'comp_id' => $model->comp_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'sales_type' => $model->sales_type, 'comp_id' => $model->comp_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sales_type',
            'sales_type_def',
            'comp_id',
        ],
    ]) ?>

</div>
