<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ReffSalesType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reff-sales-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sales_type_def')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
