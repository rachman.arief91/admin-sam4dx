<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BiCheckSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bi Check';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bi-check-index">

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

   <!--  <p>
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
 -->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No'
            ],
            [
                'attribute' => 'branchCode.branch_name',
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'cust.identification_no',
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'cust.cust_name',
                'headerOptions' => ['class' => 'text-center']

            ],
            [
                'attribute' => 'cust.cust_address',
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'target_nominal',
                'format' => 'currency',
                'contentOptions' => ['class' => 'text-right'],
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'prospect_crtdt',
                'format' => 'date',
                'header' => 'Tanggal Prospek',
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'template' => '{update}'
            ],
        ],
    ]); ?>
</div>
