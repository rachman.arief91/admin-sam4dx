<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BiCheck */

$this->title = 'Create Bi Check';
$this->params['breadcrumbs'][] = ['label' => 'Bi Checks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bi-check-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
