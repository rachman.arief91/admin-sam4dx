<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product__id')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'product_type')->widget(Select2::className(),[
            'model' => $model,
            'attribute' => 'product_type',
            'data' => ArrayHelper::map(\common\models\ReffProductType::find()->all(),'product_type','product_type_def'),
            'options' => ['placeholder' => 'Choose Product Type'],
            'pluginOptions' => [
                    'allowClear' => true
            ]
    ]) ?>

    <?= $form->field($model, 'sales_type')->widget(Select2::className(),[
            'model' => $model,
            'attribute' => 'sales_type',
            'data' => ArrayHelper::map(\common\models\ReffSalesType::find()->all(),'sales_type','sales_type_def'),
            'options' => ['placeholder' => 'Choose Sales Type'],
            'pluginOptions' => [
                    'allowClear' => true
            ]
    ]) ?>

    <?= $form->field($model, 'product_def')->textInput(['maxlength' => true]) ?>

    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
