<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SamUsers */

$this->title = 'Create Sam Users';
$this->params['breadcrumbs'][] = ['label' => 'Sam Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sam-users-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
