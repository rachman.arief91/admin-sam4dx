/**
 * Created by rachmanareef on 2/6/18.
 */
$('i.glyphicon-refresh-animate').hide();

var _data;

function refresh(spv) {
    return $.ajax({
        url: "feed?spv="+spv,
        async: false
    }).responseJSON;

}

$('.search[data-target]').keyup(function () {
    search($(this).data('target'));
});

$('.assign').click(function () {
    var $this = $(this);
    var availData = $('select.list[data-target="available"]').val();
    $.post('assign',{avail: availData, spv: _spv }, function (r) {
        _data=refresh(_spv);
        search('available');
        search('assigned');
        console.log(r);
    });

});

$('.revoke').click(function () {
    var $this = $(this);
    var assignData = $('select.list[data-target="assigned"]').val();
    $.post('revoke',{assign: assignData }, function (r) {
        _data = refresh(_spv);
        search('available');
        search('assigned');
        console.log(r);
    });
});

function search(target) {
    var $list = $('select.list[data-target="' + target + '"]');
    $list.html('');
    var q = $('.search[data-target="' + target + '"]').val();
    $.each(_data.model[target], function () {
        var r = this;
        $('<option>').text(r.nama).val(r.user_id).appendTo($list);

    });
}


_data = refresh(_spv);
search('available');
search('assigned');