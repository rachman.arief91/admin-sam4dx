<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SamUsers */

$this->title = $model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'Sam Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sam-users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->user_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_id',
            'role_id',
            'email:email',
            'passwd',
            'nama',
            'address',
            'phone_no',
            'cur_lat',
            'cur_lon',
            'fbs_token',
            'profile_pic',
            'active:boolean',
            'user_status',
            'fcm_token',
            'token',
            'branch_code',
            'comp_id',
            'sales_type',
        ],
    ]) ?>

</div>
