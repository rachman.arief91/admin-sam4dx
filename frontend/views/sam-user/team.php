<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 2/6/18
 * Time: 2:59 PM
 */
use yii\helpers\Html;
use \yii\helpers\Json;
use yii\widgets\DetailView;


$this->title = 'Assignment Team';
$this->params['breadcrumbs'][] = ['label' => 'Sam Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$animateIcon = ' <i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i>';
$this->registerJs("var _spv = '{$id}';");
$this->registerJs($this->render('_script.js'));

?>

<?= DetailView::widget([
    'model' => $user,
    'attributes' => [
        'user_id'
    ],
]) ?>

<br>
<br>
<div class="row">
    <div class="col-sm-5">
        <select multiple size="20" class="form-control list" data-target="available"></select>
    </div>
    <div class="col-sm-1">
        <br><br>
        <button type="button" class="btn btn-primary assign">
            >>
        </button>
        <br><br>
        <button type="button" class="btn btn-danger revoke">
            <<
        </button>
    </div>
    <div class="col-sm-5">
        <select multiple size="20" class="form-control list" data-target="assigned"></select>

    </div>
</div>
