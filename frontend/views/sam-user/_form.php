<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\SamRole;
use common\models\Branch;

/* @var $this yii\web\View */
/* @var $model common\models\SamUsers */
/* @var $form yii\widgets\ActiveForm */
$session = Yii::$app->session;
?>

<div class="sam-users-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'role_id')->widget(Select2::className(),[
        'id' => 'role_id',
        'model' => $model,
        'attribute' => 'product_type',
        'data' => ArrayHelper::map(SamRole::find()->all(),'role_id','role_description'),
        'options' => ['placeholder' => 'Choose Role'],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'branch_code')->widget(Select2::className(),[
        'model' => $model,
        'attribute' => 'branch_code',
        'data' => ArrayHelper::map(Branch::find()->where(['comp_id' => $session->get('comp_id')])->all(),'branch_code','branch_name'),
        'options' => ['placeholder' => 'Choose Branch'],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model,'user_id')->hiddenInput()->label(false)?>
    <?= $form->field($model,'passwd')->hiddenInput()->label(false)?>
    <?= $form->field($model,'comp_id')->hiddenInput()->label(false)?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
