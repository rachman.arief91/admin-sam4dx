<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SamUsers */

$this->title = 'Edit Users: ' . $model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'Sam Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sam-users-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
