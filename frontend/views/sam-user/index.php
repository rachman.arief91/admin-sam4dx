<?php

use yii\helpers\Html;
use yii\grid\GridView;
use mdm\admin\components\Helper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SamUsersSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sam-users-index">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user_id',
            'nama',
            [
                'attribute' => 'company',
                'value'     => 'company.company_name'
            ],
            [
                'attribute' => 'branch',
                'value'     => 'branch.branch_name'
            ],
            [
                'attribute' => 'role',
                'value'     => 'role.role_description'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'template' => '{activated} &nbsp; {edit} &nbsp; {team} &nbsp; {reset}',
                'buttons' => [
                    'activated' => function ($url, $model){
                        return ($model->active)? Html::a('<span title="Deactivated" class="fa fa-times"></span>', Url::to(['deactivated','id' => $model->user_id])) :Html::a('<span title="Activated" class="fa fa-check"></span>', Url::to(['activated','id' => $model->user_id]));
                    },
                    'edit' => function ($url, $model){
                        return Html::a('<span title="Edit" class="fa fa-pencil"></span>', Url::to(['update','id' => $model->user_id]));
                    },
                    'team' => function ($url, $model){
                        return Html::a('<span title="Edit Team" class="fa fa-users"></span>', Url::to(['team','id' => $model->user_id]));
                    },
                    'reset' => function ($url, $model){
                        return Html::a('<span title="Reset Password" class="fa fa-key"></span>', Url::to(['reset','id' => $model->user_id]));
                    }
                ]
            ],
        ],
    ]); ?>
</div>
