<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\Company;
use common\models\Branch;
use common\models\SamRole;
/* @var $this yii\web\View */
/* @var $model common\models\search\SamUsersSerach */
/* @var $form yii\widgets\ActiveForm */

$session = Yii::$app->session;
?>

<div class="sam-users-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'user_id') ?>
    <?= $form->field($model, 'nama') ?>
    <?php  echo $form->field($model, 'role')->widget(Select2::className(),[
        'model' => $model,
        'attribute' => 'branch',
        'data' => ArrayHelper::map(SamRole::find()->all(),'role_id','role_description'),
        'options' => ['placeholder' => 'Choose Role'],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?php  echo $form->field($model, 'branch')->widget(Select2::className(),[
        'model' => $model,
        'attribute' => 'branch',
        'data' => ArrayHelper::map(Branch::find()->where(['comp_id' => $session->get('comp_id')])->all(),'branch_code','branch_name'),
        'options' => ['placeholder' => 'Choose Branch'],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
