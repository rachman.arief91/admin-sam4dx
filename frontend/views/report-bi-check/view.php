<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ReportBiCheck */

$this->title = $model->prospect_id;
$this->params['breadcrumbs'][] = ['label' => 'Report Bi Checks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-bi-check-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->prospect_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->prospect_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'prospect_id',
            'branch_code',
            'user_id',
            'product_type',
            'product__id',
            'result_code',
            'status_code',
            'cust_id',
            'prospect_crtdt',
            'current_seq',
            'target_nominal',
            'prospect_type',
            'has_due_date:boolean',
            'due_date',
            'comp_id',
            'bi_check',
            'bi_note',
            'activity_code',
            'alifast_no',
            'image_url:url',
            'longitude',
            'latitude',
        ],
    ]) ?>

</div>
