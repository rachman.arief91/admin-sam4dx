<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ReffResultSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reff Result';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reff-result-index">
    <p>
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'result_def',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
