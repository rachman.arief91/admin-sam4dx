<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ReffResult */

$this->title = 'Create Reff Result';
$this->params['breadcrumbs'][] = ['label' => 'Reff Results', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reff-result-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
