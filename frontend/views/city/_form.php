<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'city_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'province_code')->widget(Select2::className(),[
            'model' => $model,
            'attribute' => 'province_code',
            'data' => ArrayHelper::map(\common\models\Province::find()->all(),'province_code','province_name'),
            'options' => ['placeholder' => 'Choose Province'],
            'pluginOptions' => [
                    'allowClear' => true
            ]
    ]) ?>

    <?= $form->field($model, 'city_name')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'rank')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
