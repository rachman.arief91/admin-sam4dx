<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ReffCompanyType */

$this->title = 'Update Reff Company Type';
$this->params['breadcrumbs'][] = ['label' => 'Reff Company Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->company_type, 'url' => ['view', 'id' => $model->company_type]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reff-company-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
