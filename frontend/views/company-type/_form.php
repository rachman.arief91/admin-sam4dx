<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ReffCompanyType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reff-company-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'company_type_def')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
