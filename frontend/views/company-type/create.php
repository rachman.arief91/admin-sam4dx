<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ReffCompanyType */

$this->title = 'Create Reff Company Type';
$this->params['breadcrumbs'][] = ['label' => 'Reff Company Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reff-company-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
