<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ReffActType */

$this->title = 'Update Reff Act Type';
$this->params['breadcrumbs'][] = ['label' => 'Reff Act Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->act_type, 'url' => ['view', 'id' => $model->act_type]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reff-act-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
