<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ReffActTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reff Action Type';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reff-act-type-index">
    <p>
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
            'act_type_def',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
