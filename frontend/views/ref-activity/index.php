<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\RefActivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Activity';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-activity-index">

    <p>
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
                'activity_name',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
