<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\BranchSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-6">
        <section class="tile">
        <div class="tile-header dvd dvd-btm">
            <h1 class="custom-font"><strong>Search</strong> Branch</h1>

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'branch_code') ?>

    <?= $form->field($model, 'comp_id') ?>

    <?= $form->field($model, 'province_code') ?>

    <?= $form->field($model, 'city_code') ?>

    <?= $form->field($model, 'branch_name') ?>

    <?php // echo $form->field($model, 'branch_address') ?>

    <?php // echo $form->field($model, 'branch_lat') ?>

    <?php // echo $form->field($model, 'branch_lon') ?>

    <?php // echo $form->field($model, 'parent_branch') ?>

    <?php // echo $form->field($model, 'branch_type') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
        </div>
        </div>
    </div>
</div>
