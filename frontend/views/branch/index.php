<?php

use yii\helpers\Html;
use \yii\widgets\Pjax;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\BranchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Branch';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page branch-index">

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin()?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'branch_name',
                'header' => 'Cabang'
            ],
            [
                'attribute' => 'branch_address',
                'header' => 'Alamat'
            ],
            [
                'attribute' => 'city.city_name',
                'header' => 'Kota'
            ],
            [
                'attribute' => 'province.province_name',
                'header' => 'Provinsi'
            ],
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end()?>
</div>
