<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Branch */

$this->title = $model->branch_code;
$this->params['breadcrumbs'][] = ['label' => 'Branches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="branch-view">

    <p>
        <?= Html::a('Update', ['update', 'branch_code' => $model->branch_code, 'comp_id' => $model->comp_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'branch_code' => $model->branch_code, 'comp_id' => $model->comp_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'branch_code',
            'comp_id',
            'province_code',
            'city_code',
            'branch_name',
            'branch_address',
            'branch_lat',
            'branch_lon',
            'parent_branch',
            'branch_type',
        ],
    ]) ?>

</div>
