<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\Province;
use common\models\BranchType;
/* @var $this yii\web\View */
/* @var $model common\models\Branch */
/* @var $form yii\widgets\ActiveForm */
$session = Yii::$app->session;
?>

<div class="branch-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'branch_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comp_id')->hiddenInput(['value' => $session->get('comp_id')])->label(false) ?>

    <?=$form->field($model,'province_code')->dropDownList(ArrayHelper::map(Province::find()->all(), 'province_code', 'province_name'),['id' => 'province_code','prompt' => 'Choose Province'])?>

    <?= $form->field($model, 'city_code')->widget(DepDrop::className(),[
        'options' => ['id' => 'city_code'],
        'pluginOptions' => [
            'depends' => ['province_code'],
            'url' => \yii\helpers\Url::to(['branch/city'])
        ]
    ]) ?>

    <?= $form->field($model, 'branch_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'branch_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'branch_lat')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'branch_lon')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'parent_branch')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'branch_type')->dropDownList(ArrayHelper::map(BranchType::find()->all(),'branch_type','branch_type_def')) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
