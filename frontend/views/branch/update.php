<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Branch */

$this->title = 'Update Branch: ' . $model->branch_code;
$this->params['breadcrumbs'][] = ['label' => 'Branches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->branch_code, 'url' => ['view', 'branch_code' => $model->branch_code, 'comp_id' => $model->comp_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="branch-update"> 

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
