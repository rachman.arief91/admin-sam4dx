<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/vendor/bootstrap.css',
        'css/vendor/animate.css',
        'css/vendor/font-awesome.css',
        'css/main.css',
        'css/vendor/animate.css',
//        'css/vendor/font-awesome.min.css',
        'css/vendor/simple-line-icons.css',
//        'css/vendor/animsition/animsition.min.css',
        'css/vendor/datatables/semantic.css',
        'css/vendor/datatables/dataTables.semanticui.css',
        'css/vendor/daterangepicker/daterangepicker-bs3.css',
        'css/vendor/datetimepicker/bootstrap-datetimepicker.min.css',
        'css/vendor/toastr/toastr.min.css',
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'
//

    ];
    public $js = [
        'js/vendor/bootstrap.min.js',
        'js/vendor/jRespond.min.js',
        'js/vendor/jquery.sparkline.min.js',
        'js/vendor/jquery.slimscroll.min.js',
        'js/vendor/jquery.animsition.min.js',
        'js/vendor/screenfull.min.js',
        'js/vendor/parsley.min.js',

        'js/vendor/toastr.min.js',
        'js/vendor/semantic.min.js',

        'js/minovate.js',
        'js/function.js',
        'js/main.js',
        'js/moment.min.js',
        'js/autoNumeric.js',
        'js/popup.js'

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
