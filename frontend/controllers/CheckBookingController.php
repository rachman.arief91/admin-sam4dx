<?php

namespace frontend\controllers;

use common\models\Activity;
use common\models\Alifast;
use common\models\search\ListApprovalSearch;
use Yii;
use common\models\CheckBooking;
use common\models\search\CheckBookingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CheckBookingController implements the CRUD actions for CheckBooking model.
 */
class CheckBookingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CheckBooking models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CheckBookingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CheckBooking model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CheckBooking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CheckBooking();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CheckBooking model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CheckBooking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CheckBooking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CheckBooking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CheckBooking::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionApprove(){
        $sesion = Yii::$app->session;
        $request = Yii::$app->request;
        $id_prospect = $request->get('id_prospect');
        $alifast = new Alifast();
            if($alifast->load(Yii::$app->request->post()) && $alifast->validate()){
                $prospect = CheckBooking::findOne($id_prospect);
                $prospect->status_code = 3;

                if($prospect->save()){
                    $activity = Activity::findOne(['prospect_id' => $id_prospect,'status_code' => 4]);
                    $activity->status_code = 3;
                    $activity->alifast_no = $alifast->alifast_no;
                    $activity->save();
                    $sesion->setFlash('approve');
                    return $this->redirect(['index']);
                }
            }
        return $this->render('alifast_approve',['model' => $alifast]);
    }

    public function actionReject(){
        $sesion = Yii::$app->session;
        $request = Yii::$app->request;
        $id_prospect = $request->get('id_prospect');
        $alifast = new Alifast();
        if($alifast->load(Yii::$app->request->post()) && $alifast->validate()){
            $prospect = CheckBooking::findOne($id_prospect);
            $prospect->status_code = 5;

            if($prospect->save()){
                $activity = Activity::findOne(['prospect_id' => $id_prospect,'status_code' => 4]);
                $activity->status_code = 5;
                $activity->alifast_no = $alifast->alifast_no;
                $activity->save();
                $sesion->setFlash('reject');
                return $this->redirect(['index']);
            }
        }
        return $this->render('alifast_reject',['model' => $alifast]);
    }


    public function actionListApproval(){
        $searchModel = new ListApprovalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,1);

        return $this->render('list_approval', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListRejected(){
        $searchModel = new ListApprovalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,2);

        return $this->render('list_rejected', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjax(){

    }
}
