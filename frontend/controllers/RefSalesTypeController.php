<?php

namespace frontend\controllers;

use Yii;
use common\models\ReffSalesType;
use common\models\search\ReffSalesTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefSalesTypeController implements the CRUD actions for ReffSalesType model.
 */
class RefSalesTypeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReffSalesType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReffSalesTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single ReffSalesType model.
     * @param integer $sales_type
     * @param string $comp_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($sales_type, $comp_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($sales_type, $comp_id),
        ]);
    }

    /**
     * Creates a new ReffSalesType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ReffSalesType();
        $session = Yii::$app->session;

        if ($model->load(Yii::$app->request->post())) {
            $model->comp_id = $session->get('comp_id');
            $model->save();
            return $this->redirect('index');
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Updates an existing ReffSalesType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $sales_type
     * @param string $comp_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($sales_type, $comp_id)
    {
        $model = $this->findModel($sales_type, $comp_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ReffSalesType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $sales_type
     * @param string $comp_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($sales_type, $comp_id)
    {
        $this->findModel($sales_type, $comp_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ReffSalesType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $sales_type
     * @param string $comp_id
     * @return ReffSalesType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($sales_type, $comp_id)
    {
        if (($model = ReffSalesType::findOne(['sales_type' => $sales_type, 'comp_id' => $comp_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
