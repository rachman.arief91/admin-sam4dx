<?php

namespace frontend\controllers;

use common\models\Activity;
use common\models\AuthAssignment;
use common\models\Prospect;
use common\models\Register;
use common\models\SalesTarget;
use common\models\SamRole;
use common\models\Team;
use common\models\User;
use frontend\components\Curl;
use Yii;
use common\models\SamUsers;
use common\models\search\SamUsersSerach;
use yii\base\ErrorException;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\IntegrityException;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\components\UUID;
use yii\web\ServerErrorHttpException;

/**
 * SamUserController implements the CRUD actions for SamUsers model.
 */
class SamUserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SamUsers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SamUsersSerach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SamUsers model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SamUsers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $session = Yii::$app->session;
        $register = new Register();
        $user = new User();
        $connection = Yii::$app->db;
        //$transaction = $connection->beginTransaction();

            if ($register->load(Yii::$app->request->post()) && $register->validate()) {
                $user->username = $register->username;
                $user->email = $register->email;
                $user->setPassword($register->password);
                $user->generateAuthKey();
                $model = new SamUsers();
                if ($user->save() && $user->validate()) {
                    $model->user_id = trim($user->username);
                    $model->nama = $register->name;
                    $model->passwd = $this->generateEncrypt($register->password);
                    $model->email = $user->email;
                    $model->phone_no = $register->phone;
                    $model->address = $register->address;
                    $model->role_id = $register->role;
                    $model->branch_code = $register->branch;
                    $model->comp_id = '53af';
                    $model->sales_type = $register->salesType;
                    $model->active = true;
                    if ($model->validate()) {

                        //Batch processs here to insert target, for each new user assign as sales role
                        if ($model->role_id == 1) {
                            $currentMonth = new Expression("EXTRACT (MONTH FROM NOW())");
                            $currentYear = new Expression("EXTRACT (YEAR FROM NOW())");
                            $month = (new \yii\db\Query)->select($currentMonth)->scalar();
                            $year = (new \yii\db\Query)->select($currentYear)->scalar();

                                for ($i = $month; $i <= 12; $i++) {
                                    $product = SalesTarget::find()->select(['product_id'])->where(['branch_code' => $model->branch_code])->distinct()->all();
                                    foreach ($product as $product_){
                                        $connection->createCommand()->insert('sales_target',[
                                            'sales_id' => $model->user_id,
                                            'comp_id' => $session->get('comp_id'),
                                            'product_id' => $product_->product_id,
                                            'target_nominal' => 0,
                                            'bobot' => 25,
                                            'year' => $year,
                                            'month' => $i,
                                            'assignee' => Yii::$app->user->identity->username,
                                            'branch_code' => $model->branch_code
                                        ])->execute();
                                    }
                                }

                            $count = SalesTarget::find()->select(['sales_id'])
                                ->where(['branch_code' => $model->branch_code])->distinct()
                                ->count();

                            for ($j = $month; $j <= 12; $j++){
                                $product = SalesTarget::find()->select(['product_id'])->where(['branch_code' => $model->branch_code])->distinct()->all();
                                foreach ($product as $product_){
                                    $sum = SalesTarget::find()
                                        ->where(['month' => $month])
                                        ->andFilterWhere(['year' => $year, 'branch_code' => $model->branch_code, 'product_id' => $product_->product_id])
                                        ->sum('target_nominal');
                                    $updatedSum = $sum / $count;

                                    $connection->createCommand()->update('sales_target',
                                        ['target_nominal' => $updatedSum],
                                        ['product_id' => $product_->product_id, 'month' => $j, 'year' => $year, 'branch_code' => $model->branch_code]
                                    )->execute();
                                }
                            }

                        }
                        $model->save();
                        $auth = new AuthAssignment();
                        $role = SamRole::findOne($register->role);
                        $auth->user_id = $user->id;
                        $auth->item_name = $role->role_description;
                        $auth->created_at = time();
                        $auth->save();
                        return $this->redirect('index');
                    }
                }
            } else {
                return $this->render('register', [
                    'model' => $register,
                ]);
            }


    }

    /**
     * Updates an existing SamUsers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $session = Yii::$app->session;
            $connection = Yii::$app->db;
            $chek = SamUsers::findOne($id);
            $currentMonth = new Expression("EXTRACT (MONTH FROM NOW())");
            $currentYear = new Expression("EXTRACT (YEAR FROM NOW())");
            $month = (new \yii\db\Query)->select($currentMonth)->scalar();
            $year = (new \yii\db\Query)->select($currentYear)->scalar();

            if($chek->branch_code != $model->branch_code){

                if($model->role_id == 1){
                    //$transaction = $connection->beginTransaction();
                    $product = SalesTarget::find()->select(['product_id'])->where(['branch_code' => $chek->branch_code])->distinct()->all();
                    $sumForupdate = array();
                    $index = 0;
                    foreach ($product as $value){
                        $sum = SalesTarget::find()
                            ->where(['month' => $month])
                            ->andFilterWhere(['year' => $year, 'branch_code' => $chek->branch_code, 'product_id' => $value->product_id])
                            ->sum('target_nominal');

                        $sumForupdate[$index] = $sum;
                        $index++;
                    }

                    $connection->createCommand()->delete('sales_target',['sales_id' => $model->user_id])->execute();
                    $count = SalesTarget::find()->select(['sales_id'])
                            ->where(['branch_code' => $chek->branch_code])->distinct()
                            ->count();

                            for ($j = $month; $j <= 12; $j++){
                                $product = SalesTarget::find()->select(['product_id'])->where(['branch_code' => $chek->branch_code])->distinct()->all();
                                $index = 0;
                                foreach ($product as $product_){
                                    $updatedSum = $sumForupdate[$index] / $count;
                                    $connection->createCommand()->update('sales_target',
                                        ['target_nominal' => $updatedSum],
                                        ['product_id' => $product_->product_id, 'month' => $j, 'year' => $year, 'branch_code' => $chek->branch_code]
                                    )->execute();

                                    $index++;
                                }
                            }

                            for ($i = $month; $i <= 12; $i++) {
                                $product = SalesTarget::find()->select(['product_id'])->where(['branch_code' => $model->branch_code])->distinct()->all();

                                foreach ($product as $product_){
                                    $connection->createCommand()->insert('sales_target',[
                                        'sales_id' => $model->user_id,
                                        'comp_id' => $session->get('comp_id'),
                                        'product_id' => $product_->product_id,
                                        'target_nominal' => 0,
                                        'bobot' => 25,
                                        'year' => $year,
                                        'month' => $i,
                                        'assignee' => Yii::$app->user->identity->username,
                                        'branch_code' => $model->branch_code
                                    ])->execute();
                                }
                            }


                        $count = SalesTarget::find()->select(['sales_id'])
                            ->where(['branch_code' => $model->branch_code])->distinct()
                            ->count();


                            for ($u = $month; $u <= 12; $u++){
                                $product = SalesTarget::find()->select(['product_id'])
                                    ->where(['branch_code' => $model->branch_code])
                                    ->distinct()->all();
                                foreach ($product as $product_){
                                    $sum = SalesTarget::find()
                                        ->where(['month' => $month])
                                        ->andFilterWhere(['year' => $year, 'branch_code' => $model->branch_code, 'product_id' => $product_->product_id])
                                        ->sum('target_nominal');

                                    $updatedSum = $sum / $count;

                                    $connection->createCommand()->update('sales_target',
                                        ['target_nominal' => $updatedSum],
                                        ['product_id' => $product_->product_id, 'month' => $u, 'year' => $year, 'branch_code' => $model->branch_code]
                                    )->execute();
                                }
                            }


                }
            }

            if($chek->role_id != $model->role_id){
                if($model->role_id == 2){
                    $product = SalesTarget::find()->select(['product_id'])->where(['branch_code' => $chek->branch_code])->distinct()->all();
                    $sumForupdate = array();

                    $index = 0;
                    foreach ($product as $value){
                        $sum = SalesTarget::find()
                            ->where(['month' => $month])
                            ->andFilterWhere(['year' => $year, 'branch_code' => $chek->branch_code, 'product_id' => $value->product_id])
                            ->sum('target_nominal');

                        $sumForupdate[$index] = $sum;
                        $index++;
                    }

                    $connection->createCommand()->delete('sales_target',['sales_id' => $model->user_id])->execute();
                    $count = SalesTarget::find()->select(['sales_id'])
                        ->where(['branch_code' => $chek->branch_code])->distinct()
                        ->count();

                    for ($j = $month; $j <= 12; $j++){
                        $product = SalesTarget::find()->select(['product_id'])->where(['branch_code' => $chek->branch_code])->distinct()->all();
                        $index = 0;
                        foreach ($product as $product_){
                            $updatedSum = $sumForupdate[$index] / $count;
                            $connection->createCommand()->update('sales_target',
                                ['target_nominal' => $updatedSum],
                                ['product_id' => $product_->product_id, 'month' => $j, 'year' => $year, 'branch_code' => $chek->branch_code]
                            )->execute();

                            $index++;
                        }

                    }

                }
            }

            $model->save();

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SamUsers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SamUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return SamUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SamUsers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function generateEncrypt($plain_text)
    {
        $sha1pass = sha1($plain_text);
        $curl = new Curl();
        $param = [
            'plaintext' => $sha1pass
        ];

        $response = $curl->setRequestBody(json_encode($param))->setHeaders([
            'Content-Type' => 'application/json'
        ])->post("http://localhost:8080/encrypt");

        $decode_value = json_decode($response);
        return $decode_value->encrypttext;
    }

    public function actionTeam()
    {
        $id = Yii::$app->request->get('id');
        $user = SamUsers::findOne($id);
        return $this->render('team', [
            'user' => $user,
            'id' => $id
        ]);
    }

    public function actionFeed()
    {
        $id = Yii::$app->request->get('spv');
        $roleFilter = SamUsers::find()->where(['user_id' => $id])->one();
        $hasAssign = Team::find()->select(['sales_id'])->where(['spv_id' => $id]);

        $userAvailable = SamUsers::find()->select(['nama', 'user_id']);


        switch ($roleFilter->role_id) {
            case 5:
                $teamPair = SamUsers::find()->select(['c.sales_id'])->innerJoinWith('child c', false)->where(['role_id' => 4]);
                $userAvailable = $userAvailable->where(['role_id' => 4])->andFilterWhere(['not in', 'user_id', $teamPair]);
                break;
            case 4:
                $teamPair = SamUsers::find()->select(['c.sales_id'])->innerJoinWith('child c', false)->where(['role_id' => 3]);
                $userAvailable = $userAvailable->where(['role_id' => 3])->andFilterWhere(['not in', 'user_id', $teamPair]);
                break;
            case 3:
                $teamPair = SamUsers::find()->select(['c.sales_id'])->innerJoinWith('child c', false)->where(['role_id' => 2]);
                $userAvailable = $userAvailable->where(['role_id' => 2])->andFilterWhere(['not in', 'user_id', $teamPair])->andFilterWhere(['branch_code' => $roleFilter->branch_code]);
                break;
            case 2:
                $teamPair = SamUsers::find()->select(['c.sales_id'])->innerJoinWith('child c', false)->where(['role_id' => 1]);
                $userAvailable = $userAvailable->where(['role_id' => 1])->andFilterWhere(['not in', 'user_id', $teamPair])->andFilterWhere(['branch_code' => $roleFilter->branch_code]);
                break;
        }

        $userAvailable = $userAvailable->orderBy('nama')->asArray()->all();
        $userAssign = SamUsers::find()->select(['nama', 'user_id'])
            ->joinWith('child c')
            ->where(['c.spv_id' => $id])
            ->orderBy('nama')
            ->asArray()->all();

        Yii::$app->getResponse()->format = 'json';

        return ['model' => [
            'available' => $userAvailable,
            'assigned' => $userAssign
        ]];

    }

    public function actionDeactivated($id)
    {
        $model = $this->findModel($id);
        $currentMonth = new Expression("EXTRACT (MONTH FROM NOW())");
        $currentYear = new Expression("EXTRACT (YEAR FROM NOW())");
        $month = (new \yii\db\Query)->select($currentMonth)->scalar();
        $year = (new \yii\db\Query)->select($currentYear)->scalar();
        $session = Yii::$app->session;
        $connection = Yii::$app->db;

        if($model->role_id == 1){
            $product = SalesTarget::find()->select(['product_id'])->where(['branch_code' => $model->branch_code])->distinct()->all();
            $sumForupdate = array();

            $index = 0;
            foreach ($product as $value){
                $sum = SalesTarget::find()
                    ->where(['month' => $month])
                    ->andFilterWhere(['year' => $year, 'branch_code' => $model->branch_code, 'product_id' => $value->product_id])
                    ->sum('target_nominal');

                $sumForupdate[$index] = $sum;
                $index++;
            }

            $connection->createCommand()->delete('sales_target',['sales_id' => $model->user_id])->execute();
            $count = SalesTarget::find()->select(['sales_id'])
                ->where(['branch_code' => $model->branch_code])->distinct()
                ->count();

            for ($j = $month; $j <= 12; $j++){
                $product = SalesTarget::find()->select(['product_id'])->where(['branch_code' => $model->branch_code])->distinct()->all();
                $index = 0;
                foreach ($product as $product_){
                    $updatedSum = $sumForupdate[$index] / $count;
                    $connection->createCommand()->update('sales_target',
                        ['target_nominal' => $updatedSum],
                        ['product_id' => $product_->product_id, 'month' => $j, 'year' => $year, 'branch_code' => $model->branch_code]
                    )->execute();

                    $index++;
                }
            }

            $prospect = Prospect::find()->where(['user_id' => $model->user_id])->all();

            foreach ($prospect as $prospects){
                if($prospects->status_code == 0){
                    $connection->createCommand()->update('prospect',['status_code' => 2], ['status_code' => 0, 'prospect_id' => $prospects->prospect_id])->execute();
                    $max = Activity::find()->select('max(act_seq)')->where(['prospect_id'=> $prospects->prospect_id])->scalar();
                    $connection->createCommand()->update('activity',['status_code' => 2], ['status_code' => 0, 'prospect_id' => $prospects->prospect_id,'act_seq'=>$max])->execute();
                }
            }

        }
        $model->active = false;
        $model->save();

        return $this->redirect(['index']);
    }

    public function actionActivated($id)
    {
        $model = $this->findModel($id);
        $model->active = true;

        $model->save();

        return $this->redirect(['index']);
    }

    public function actionAssign()
    {
        $avail = Yii::$app->getRequest()->post('avail', []);
        $spv = Yii::$app->getRequest()->post('spv');


        $now = new Expression('NOW()');
        foreach ($avail as $data) {
            $assign = new Team();
            $assign->team_id = UUID::v4();
            $assign->spv_id = $spv;
            $assign->active = true;
            $assign->sales_id = $data;
            $assign->crtdt = $now;
            $assign->save();
        }

        Yii::$app->getResponse()->format = 'json';
        return $avail;
    }

    public function actionRevoke()
    {
        $assign = Yii::$app->getRequest()->post('assign', []);
        Team::deleteAll(['sales_id' => $assign]);
        Yii::$app->getResponse()->format = 'json';
        return $assign;
    }

    public function actionReset($id)
    {
        $model = $this->findModel($id);
        $model->passwd = '123123';

        $model->save();

        return $this->redirect(['index']);
    }


}
