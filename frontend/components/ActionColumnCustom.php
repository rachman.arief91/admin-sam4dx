<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 1/31/18
 * Time: 5:53 PM
 */

namespace frontend\components;


use yii\grid\ActionColumn;
use mdm\admin\components\Helper;
use yii\helpers\Url;
use yii\helpers\Html;

class ActionColumnCustom extends ActionColumn
{
    public function init()
    {
        parent::init();
        $this->initDefaultButtons();
        $this->header = 'Action';
        $this->headerOptions = ['class' => 'text-center'];
       /* if ($this->template == '{view} {update} {delete}')
            $this->template = Helper::filterActionColumn('{view} {update} {disable} {enable} {delete} {restore}');*/

        $this->buttons = array_merge($this->buttons,[
            'view' => function ($url, $model) {
                return '<a href="'.Url::to(['view', 'id' => $model->user_id]).'"><span title="View Detail" class="label label-primary"><i class="fa fa-eye"></i></span></a>';
            }
        ]);
    }


}